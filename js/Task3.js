"use strict";

/* 3.Опціональне. Завдання:
Реалізувати повне клонування об'єкта. 

Технічні вимоги:
- Написати функцію для рекурсивного повного клонування об'єкта (без єдиної передачі за посиланням, внутрішня вкладеність властивостей об'єкта може бути досить великою).
- Функція має успішно копіювати властивості у вигляді об'єктів та масивів на будь-якому рівні вкладеності.
- У коді не можна використовувати вбудовані механізми клонування, такі як функція Object.assign() або spread.
 */


const testObject = {
    a: 1,
    d: "Porn",
}

const objectForCopying = {
    a: 3,
    b: {
        c: "trip",
        d: {
            e: true,
            f: {
                g: 24,
            }
        }
    }
}

const getCopiedFunction = (objectForCopying) => {  //function for making copy

    const clonedObj = {};

    for (let key in objectForCopying) {
        if (typeof objectForCopying[key] === "object") {
            clonedObj[key] = getCopiedFunction(clonedObj[key]);
        } else {
            clonedObj[key] = objectForCopying[key];
        }
    }
    return clonedObj;
}

const clonedObj = getCopiedFunction(testObject);

console.log(clonedObj);
