"use strict";

/* Теоретичні питання
 1. Опишіть своїми словами, що таке метод об'єкту
 2. Який тип даних може мати значення властивості об'єкта?
 3. Об'єкт це посилальний тип даних. Що означає це поняття?

 Практичні завдання
 1. Створіть об'єкт product з властивостями name, price та discount. 
 Додайте метод для виведення повної ціни товару з урахуванням знижки. 
 Викличте цей метод та результат виведіть в консоль. */


 console.log("1. Це звичайна функція, але яка оголошується в обєкті.");
 console.log("2. Ключ може бути символом або рядком, а його значення може мати будь який тип");
 console.log("3. Для об'єкта виділяється окрема частина памяті. Але, коли ми присвоюємо змінній ім'я нашого обєекта - ми отримаємо посилання на нього. Грубо кажучи, ми будемо брати інфу з тієї комірки памяті, яка була зарезервована для цього обєекту");

 console.log("PRACTICE");

 console.log("TASK 1");

let productName = prompt("Enter a name");
let productPrice = prompt("Enter the price");

 const product = {
    productName,
    productPrice,
    productDiscount: 15,
    calcFullPrice () {
        let fullPrice;
        if (this.productName === "Laptop") {
            console.log("The price for it is: ", fullPrice = productPrice*0.85);
        } else {
            console.log("The price for it is: ", fullPrice = productPrice*0.75);
        }
        return fullPrice;
    },
 }

 console.log(product.calcFullPrice(this.fullPrice));