"use strict";

/*  2. Напишіть функцію, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком, 
 наприклад "Привіт, мені 30 років". Попросіть користувача ввести своє ім'я та вік 
 за допомогою prompt, і викличте функцію з введеними даними. Результат виклику функції виведіть з допомогою alert.
*/

const objTest = () => {

    const name = prompt("Enter your name");
    const age = prompt("Enter your age");
    const objectTask = {
        name,
        age,
        writtenFunction () {
            console.log((`Привіт, я ${this.name} і мені ${this.age} років`));
            alert(`Привіт, я ${this.name} і мені ${this.age} років`);
        },
        
    }
    return objectTask;
}

const user1 = objTest();

user1.writtenFunction();

